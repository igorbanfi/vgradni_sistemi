#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include<fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>

#define PORT_NUMBER 55000
#define BUF_LEN 1000000

int main( int argc, char **argv )
{
  // Preverimo če je uporabnik vpisal ime strežnika
  if( argc != 2 ){
    printf("Uporaba: %s ime_streznika\n", argv[0]);
    exit( 1 );
  }

  // priprava spremenljivk za deo s socketom
  int n, sd, initClient( char * );

  // pripravimo pomnilnik, kamor bomo shranjevali prejete podatke
  char* buf;
  buf = (char*) malloc(BUF_LEN);

  // Priprava dototeke, kamor bomo shranjevali prejete podatke
  int destfile;
  char * file_path = "video_fifo_1";
  destfile = open(file_path, O_WRONLY);

  // Incializiramo socket
  if( (sd = initClient( argv[1] )) < 0 ){
    printf("napaka init\n");  exit( 1 );
  }

  // Začnemo neskončno zanko, kjer preberemo podatke in jih zapišemo v fifo dototeko
  while (n = read(sd, buf, BUF_LEN)){
      if( write(destfile, buf, n) == -1)  perror("write err");
    }
  close( sd );
  exit( 0 );
}

int initClient( char *hostName )
{
  // Pripravimo spremenljivke za delo z socketom
  struct sockaddr_in  servAddr;
  struct hostent     *host;
  int    sd;

  // z gethostbyname incializiramo hostent, ki ga rabimo da pripravimo servAddr,
  // ki ga potrebujemo za delo z socketom, v našem primeru smo namesto hostname uporabili
  // kar ipv4 addres serverja
  if( ( host = gethostbyname( hostName )) == NULL) return( -1 );
  memset( &servAddr, 0, sizeof(servAddr));
  memcpy( &servAddr.sin_addr, host->h_addr, host->h_length );
  servAddr.sin_family = host->h_addrtype;
  servAddr.sin_port   = htons( PORT_NUMBER );

  printf("streznik: %s, ", host -> h_name);
  printf("%s:%d\n", inet_ntoa( servAddr.sin_addr ), PORT_NUMBER);
  // Incializiramo socket,
  // AF_INET -- uporabimo ipv4 protokol
  // SOCK_STREAM -- uporabljamo TCP
  if( (sd = socket(AF_INET,SOCK_STREAM,0)) < 0 ) return( -2 );

  // Incializiramo povezavo na socketu z serverjem
  if( connect(sd, (struct sockaddr *)&servAddr,sizeof(servAddr)) < 0) return( -3 );
  return( sd );
}
