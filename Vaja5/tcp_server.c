#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include<fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>

#define PORT_NUMBER 55000
#define BUF_LEN 1000000

int main( int argc, char **argv )
{
  // Preverimo če je uporabnik vpisal dovolj podatkov
  if( argc != 2 ){
    printf("Uporaba: %s ime_naprave\n", argv[0]);
    exit( 1 );
  }

  // Pripravimo spremenljivke za delo z socketom
  struct sockaddr_in cliAddr;
  socklen_t size;
  int       initServer( char * );
  int       sd, sn, n;

  // Pripravimo za delo z fifo doteteko
  int sourcefile;
  char * file_path = "video_fifo";
  sourcefile = open(file_path, O_RDONLY);

  // Incializiramo pomnilnik, kamor bomo shranjevali podatke s kamere
  char* buf;
  buf = (char*) malloc(BUF_LEN);

  // incializiramo server
  if( (sd = initServer( argv[1] )) < 0){
    printf("Napaka: init server\n"); exit( 1 );
  }

  // Poslušamo, če se želi client povezati na socket
  listen( sd, 5 );
  alarm( 60 ); /* koncaj po eni minuti */

  // Začnemo neskončno zanko
  while( 1 ){
    size = sizeof(cliAddr);
    memset( &cliAddr, 0, size );

    // Sprejmi povezavo na socket
    if( (sn = accept(sd, (struct sockaddr *)&cliAddr, &size)) < 0){
      perror("accept err"); exit( 2 );
    }
    /* zveza je vzpostavljena*/
    // Z fork naredimo novi process, ki je namenjen povezavi
    if( fork() == 0 ){
      printf("odjemalec: %s:%d\n", inet_ntoa( cliAddr.sin_addr ), ntohs( cliAddr.sin_port ) );

      // Preberemo podatke s fifo dototekee, in jo takoj zapišemo v socket
      while( (n = read( sourcefile, buf, BUF_LEN)) > 0 ){
        if( write(sn, buf, n) == -1)
          perror("write err");
      }
      printf("odjemalec: %s:%d je prekinil povezavo\n", inet_ntoa( cliAddr.sin_addr ), ntohs( cliAddr.sin_port ));
      close( sn );
      exit( 0 );
    }
  }
}

int initServer( char *hostName )
{
  // Priprava spremenljivk za delo s socketom
  struct sockaddr_in  servAddr;
  struct hostent     *host;
  int    sd;

  // z gethostbyname incializiramo hostent, ki ga rabimo da pripravimo servAddr,
  // ki ga potrebujemo za delo z socketom, v našem primeru smo namesto hostname uporabili
  // kar ipv4 addres serverja
  if( (host = gethostbyname( hostName )) == NULL) return( -1 );
  memset( &servAddr, 0, sizeof(servAddr) );
  memcpy( &servAddr.sin_addr, host->h_addr, host->h_length );
  servAddr.sin_family  = host->h_addrtype;
  servAddr.sin_port    = htons( PORT_NUMBER );

  printf("streznik: %s, ", host -> h_name);
  printf("%s:%d\n", inet_ntoa(servAddr.sin_addr), PORT_NUMBER );

  // Incializiramo socket,
  // AF_INET -- uporabimo ipv4 protokol
  // SOCK_STREAM -- uporabljamo TCP
  if( (sd = socket(AF_INET, SOCK_STREAM,0)) < 0 ) return( -2 );

  // Incializiramo povezavo na socketu z serverjem
  if( bind(sd, (struct sockaddr *)&servAddr, sizeof( servAddr )) < 0) return( -3 );
  return( sd );
}
