#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include<fcntl.h>
#include <sys/types.h>
#include <string.h>

#define SERVPORTNO 50000
#define BUF_LEN 4096

int main(int argc, char **argv)
{
  // Preverimo če je uporabnik vpisal IP
  if (argc != 2){
    printf("usage: udpcli <IP address>\n");
    exit(1);
  }

  // Pripravimo spremenljivke za delo z socketom
  int sockfd,n;
  struct sockaddr_in servaddr;

  // Pripravimo pomnilnik, kamor bomo shranjevali prejete podatke
  char* buf;
  buf = (char*) malloc(BUF_LEN);

  // Incializiramo diskriptor za delo s fifo dototeko, kamor bomo shranjevali prejete podatke
  int destfile;
  char * file_path = "video_fifo_1";
  destfile = open(file_path, O_WRONLY);

  // Incializiramo socket
  if( (sockfd=socket(AF_INET,SOCK_DGRAM,0)) == -1){
    perror("socket err");
    exit(1);
  }
  // incializiramo servaddr
  bzero(&servaddr,sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr=inet_addr(argv[1]);
  servaddr.sin_port=htons(SERVPORTNO);

  // Serverju pošljemo en paket, da pridobi potrebne podatke o clientu
  n = sendto(sockfd,buf,strlen(buf),0,
          (struct sockaddr *)&servaddr,sizeof(servaddr));
  if( n == -1 ){
    perror("sento err");
    exit(1);
  }

  // Začnemo neskončno zanko
  while (1){
    // Od serverja prejmemo paket in ga takoj zapišemo v fifo doteteko
    n=recvfrom(sockfd,buf,BUF_LEN,0,NULL,NULL);
    if( n == -1 ){
      perror("recvfrom err");
    }
    if( write(destfile, buf, n) == -1)  perror("write err");
    }
  exit( 0 );
}
