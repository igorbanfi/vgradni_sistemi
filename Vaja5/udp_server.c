#include <stdio.h>
#include <stdlib.h>
#include<fcntl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <string.h>

#define SERVPORTNO 50000
#define BUF_LEN 4096

int main( void )
{
  // priprava potrebnih spremenljivk
  int sockfd,n;
  struct sockaddr_in servaddr,cliaddr;
  socklen_t len;

  // odpremo fifo dototeko kamor raspi vid zapisuje video s kamere
  int sourcefile;
  char * file_path = "video_fifo";
  sourcefile = open(file_path, O_RDONLY);

  // Incializacija spomina kjer shranjujemo podatke, ki jih bomo poslali
  char* buf;
  buf = (char*) malloc(BUF_LEN);

  // Incializiramo socket
  // AF_INET -- uporabljamo ipv4 internetni protocol
  // SOCK_DGRAM -- upoorablamo UDP
  if( (sockfd=socket(AF_INET,SOCK_DGRAM,0)) == -1){
    perror("socket err");
    exit(1);
  }

  // Incializaciramo serveaddr, kjer je shranjen IP, port našega socketa
  bzero(&servaddr,sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr=htonl(INADDR_ANY);
  servaddr.sin_port=htons( SERVPORTNO );

  // servaddr vežemo na naš socket sockfd
  if( bind(sockfd,(struct sockaddr *)&servaddr,sizeof(servaddr)) == -1){
    perror("bind err");
    exit(1);
  }

  // Da lahko pošljemo podadtke našemu clientu, moramo poznati ip in port našega clienta
  // To lahko izvemo tako, da client pošlje en paket serverju
  // recvfrom blokira nadalnje izvajanje programa
  len = sizeof(cliaddr);
  n = recvfrom(sockfd,buf,BUF_LEN,0,(struct sockaddr *)&cliaddr,&len);

  // Začnemo loop v katerem pošiljamo podatke clientu
  printf("Starting loop\n" );
  while( 1 ){
    // v neskončni zanki beremo podatke iz fifo dototeke, kjer raspivid shranjuje video
    while( (n = read( sourcefile, buf, BUF_LEN)) > 0 ){
      // Podatke pošljemo clientu
      if( sendto(sockfd,buf,n,0,(struct sockaddr *)&cliaddr,sizeof(cliaddr)) == -1)
        perror("write err");
    }
  }
}
