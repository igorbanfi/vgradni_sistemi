/*
 *  chardev.c: Creates a device for encrypting data with Caesar sycpher
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <asm/uaccess.h>	/* for put_user */

/*
 *  Prototypes - this would normally go in a .h file
 */
int init_module(void);
void cleanup_module(void);
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);

#define SUCCESS 0
#define DEVICE_NAME "cezar"	/* Dev name as it appears in /proc/devices   */
#define MEM_SIZE 4096 // Size of memory
#define OFFSET 2 // Offset of letters


/*
 * Global variables are declared as static, so are global within the file.
 */

// Pozicija zadnjega byta, ki smo ga zapisali
int byte_num;

// Kazalec na naš alociran spomin
char * spomin;

// Pozicija zadnjega byta, ki smo ga prebrali
int byte_num_read;

static int Major;		/* Major number assigned to our device driver */
static int Device_Open = 0;	/* Is device open?
				 * Used to prevent multiple access to device */

static struct file_operations fops = {
	.read = device_read,
	.write = device_write,
	.open = device_open,
	.release = device_release
};

/*
 * This function is called when the module is loaded
 */
int init_module(void)
{
        Major = register_chrdev(0, DEVICE_NAME, &fops);

	if (Major < 0) {
	  printk(KERN_ALERT "Registering char device failed with %d\n", Major);
	  return Major;
	}

	// Alociramo spomin
  spomin = kmalloc(MEM_SIZE, GFP_KERNEL);

	// Nastavimo začetni poziciji
  byte_num = 0;
	byte_num_read = 0;

	printk(KERN_INFO "I was assigned major number %d. To talk to\n", Major);
	printk(KERN_INFO "the driver, create a dev file with\n");
	printk(KERN_INFO "'mknod /dev/%s c %d 0'.\n", DEVICE_NAME, Major);
	printk(KERN_INFO "Try various minor numbers. Try to cat and echo to\n");
	printk(KERN_INFO "the device file.\n");
	printk(KERN_INFO "Remove the device file and module when done.\n");

	return SUCCESS;
}

/*
 * This function is called when the module is unloaded
 */
void cleanup_module(void)
{
	/*
	 * Unregister the device
	 */
	unregister_chrdev(Major, DEVICE_NAME);
}

/*
 * Methods
 */

/*
 * Called when a process tries to open the device file, like
 * "cat /dev/cezar"
 */
static int device_open(struct inode *inode, struct file *file)
{
	static int counter = 0;

	if (Device_Open)
		return -EBUSY;

	Device_Open++;
	try_module_get(THIS_MODULE);
	return SUCCESS;
}

/*
 * Called when a process closes the device file.
 */
static int device_release(struct inode *inode, struct file *file)
{
	Device_Open--;		/* We're now ready for our next caller */

	/*
	 * Decrement the usage count, or else once you opened the file, you'll
	 * never get get rid of the module.
	 */
	module_put(THIS_MODULE);

	return 0;
}

/*
 * Called when a process, which already opened the dev file, attempts to
 * read from it.
 */
static ssize_t device_read(struct file *filp,	/* see include/linux/fs.h   */
			   char *buffer,	/* buffer to fill with data */
			   size_t length,	/* length of the buffer     */
			   loff_t * offset)
{
	/*
	 * Number of bytes actually written to the buffer
	 */
	int bytes_read = 0;

	/*
	 * Actually put the data into the buffer
	 */
	while (byte_num != byte_num_read) {
		// V buffer zapišemo byte, dokler ne pridemo do byta, ki smo ga zadnjega zapisali
		put_user(spomin[byte_num_read]+OFFSET, buffer++);
		byte_num_read = byte_num_read + 1;
		byte_num_read = byte_num_read % MEM_SIZE;
		bytes_read++;
	}
	printk("%d\n", bytes_read);
	/*
	 * Most read functions return the number of bytes put into the buffer
	 */
	return bytes_read;
}

static ssize_t
device_write(struct file *filp, const char *buff, size_t len, loff_t * off)
{
	// Prebermo byte, zapisane v /dev/cezar in jih shranimo v spomin
  int i;
  for (i = 0; i<len-1; i++){
    spomin[byte_num] = buff[i];
		byte_num = byte_num+1;
    byte_num = byte_num % MEM_SIZE;
  }
  return len;
}
