#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/times.h>
#include <sys/time.h>
#include <time.h>

#define BUFFER_VHOD_LEN 640*480*3 // velikost slike zajete s kamero
#define BUFFER_IZHOD_LEN 1280*1024*4 // velikost slike ki bo prikazana na zaslonu


int prvi(int fd_vhod[2]) {
    /*
     * Prvi proces. 
     * 
     * int fd_vhod[2]: file diskriptor s katerim dostopamo do cevi
     * 
     */
     
     // incualizacija spremenljick za merjenje časa
    clock_t proc, start, end;
    
    // incializiramo spreenljivko za shranjevanje slike in alociramo
    // potreben prostor na heapu
    char *buffer;
    buffer = (char*) malloc(BUFFER_VHOD_LEN);
    
    // incializiramo spremenljivko za file diskriptorje
    int sourcefile, destfile;
    
    // zapremo prvi del cevi, saj ta proces v cev samo piše
    close(fd_vhod[0]);
    destfile = fd_vhod[1];
    
    // odpremo dototeko iz katere beremo sliko kamere
    sourcefile = open("/dev/video0",O_RDONLY);
    
    // drugi vir slike za preizkušanje
    //sourcefile = open("/dev/urandom", O_RDONLY);
    
    if(sourcefile == -1)
    {
       printf("problem opening /dev/video0. \n");
       return 1;
    }
    
    printf("starting prvi \n");

    //Shranimo prvi CPU čaas
    start = clock();
    
    while (1) {
    
    // preberemo BUFFER_VHOD_LEN število bytov in jih zapišemo v cev
    int n=read(sourcefile,buffer,BUFFER_VHOD_LEN);
    int out = write( destfile, buffer, n );
    
    // izpis za debugiranje
    //printf("%d vytes written to pipe\n");
    
    // Z lseek se prestavimo na začetek dototeke /dev/video0
    lseek(sourcefile, SEEK_SET, 0);
    
    // Shranimo čas ob koncu zanke
    end = clock( );
    
    // Izračunamo koliko časa je preteklo ter vrednost izpišemo
    proc = end - start;
    printf("Proces1: %d \n",proc);
    start = end;	
    }
    close(destfile);
    close(sourcefile);	
    free(buffer);    
    return 0;
} 



int drugi( int fd_vhod[2], int fd_izhod[2] )
{
    /*
     *  Drugi proces.
     * 
     * int fd_vhod[2]: file diskriptor s katerim dostopamo do cevi za vhod
     * int fd_izhod[2]: file diskriptor s katerim dostopamo do cevi za izhod
     * 
     */
     
     // incualizacija spremenljick za merjenje časa
    clock_t proc, start, end;
     
    // incializiramo spreenljivke za shranjevanje slik in alociramo
    // potreben prostor na heapu
    char* buffer_vhod;
    char* buffer_izhod;
    buffer_vhod = (char*) malloc(BUFFER_VHOD_LEN);
    buffer_izhod = (char*) malloc(BUFFER_IZHOD_LEN);

    // incializiramo spremenljivke za file diskriptorje
    int sourcefile, destfile;
    
    // zapremo del cevi za pisanje in shranimo diskriptor za branje
    // v spremenljivko sourcefile
    close(fd_vhod[1]);
    sourcefile = fd_vhod[0];
    
    // Zapremo del cevi za branje in shranimo diskriptor za pisanje 
    // v spremenljivko destfile
    close(fd_izhod[0]);
    destfile = fd_izhod[1];
    
    printf("starting drugi \n");
    
    // shranimo prvi CPU čas
    start = clock();
    
    while (1) {
	
    int n=read(sourcefile,buffer_vhod,BUFFER_VHOD_LEN);
    // Za vsak pixel na ekranu preverimo, če "pripada" sliki
    for (int pixel_num = 0; pixel_num < BUFFER_IZHOD_LEN/4; pixel_num++){
	if (pixel_num%1280 > 640) {
	    buffer_izhod[pixel_num*4] = 0;
	    buffer_izhod[pixel_num*4+1] = 0;
	    buffer_izhod[pixel_num*4+2] = 0;
	    buffer_izhod[pixel_num*4+3] = 0;
	}
	else if (pixel_num > 1280*480) {
	    buffer_izhod[pixel_num*4] = 0;
	    buffer_izhod[pixel_num*4+1] = 0;
	    buffer_izhod[pixel_num*4+2] = 0;
	    buffer_izhod[pixel_num*4+3] = 0;
	}
	else {
	    // zaporedno stevilo pixla na ekranu pretvorimo v zaporedno 
	    // stevilo pixla na sliki
	    int y = pixel_num/1280;
	    int x = pixel_num%1280;
	    int pixel_num_vhod = y*640+x;
	    
	    // V buffer_izhod shranimo vrednost pixla, ki ga zajamemo s 
	    // kamero, vrednost alpha zapolnimo poljubno
	    const char red = buffer_vhod[pixel_num_vhod*3];
	    const char green = buffer_vhod[pixel_num_vhod*3 + 1];
	    const char blue = buffer_vhod[pixel_num_vhod*3 + 2];
	    buffer_izhod[pixel_num*4] = blue;
	    buffer_izhod[pixel_num*4+1] = green;
	    buffer_izhod[pixel_num*4+2] = red;
	    buffer_izhod[pixel_num*4+3] = 0;
	}
    }
    
    // zapišemo v izhodno cev
    int out = write( destfile, buffer_izhod, BUFFER_IZHOD_LEN);
    
    // shranimo CPU čas ob koncu procesa
    end = clock( );
    
    // izračunamo pretečeni CPU čas
    proc = end - start;
    printf("Proces2: %d \n",proc);
    start = end;
}
    free(buffer_vhod);
    free(buffer_izhod);
    return 0;
}


int tretji(int fd_izhod[2]) {
    /*
     * Tretji proces.
     *  
     * int fd_izhod[2]: file diskriptor s katerim dostopamo do cevi za vhod
     *
     */
     
    // incualizacija spremenljick za merjenje časa
    clock_t proc, start, end;
    
    double FPS;    
    
    struct timeval tv;
    long start_real, end_real;
    
    // incializiramo spreenljivke za shranjevanje slik in alociramo
    // potreben prostor na heapu
    char* buffer;
    buffer = (char*) malloc(BUFFER_IZHOD_LEN);

    // incializiramo spremenljivke za file diskriptorje
    int sourcefile;
    int fbfd = 0;

    printf("starting");
    
    // zapremo nepotreben del cevi in shranimo drugege v spremenljivko 
    // za file diskriptor
    close(fd_izhod[1]);
    sourcefile = fd_izhod[0];
    
    // Odpremo dototeko /dev/fb0
    fbfd = open("/dev/fb0", O_RDWR);
    if (!fbfd) {
	printf("Error: cannot open framebuffer device.\n");
	return 1;
    }
    
    // shranio prvi realni čas
    gettimeofday(&tv, NULL);
    start_real = tv.tv_usec + 1000000*tv.tv_sec;
    
    // shranimo prvi CPU čas
    start = clock();
   
    while (1) {
    
    // Beremo z 
    int n=read(sourcefile,buffer,BUFFER_IZHOD_LEN);
    int out = write(fbfd, buffer, n);
    
    //printf("%d bytes written to fb0 \n", out);
    
    // z lseek se prestavimo na začetek /dev/fb0
    lseek(fbfd, SEEK_SET, 0);
    
    // shranimo CPU čas ob koncu zanke
    end = clock();
    
    // shranimo realni čas ob koncu zanke
    //end_real = times(NULL);
    gettimeofday(&tv, NULL);
    end_real = tv.tv_usec + 1000000*tv.tv_sec;
    
    // iz razlik realnega časa izračunamo FPS
    FPS = 1./(double)(end_real - start_real)*1000000;
    printf("FPS: %f \n",FPS);
    start_real = end_real;
        
    // izračunamo koliko CPU časa je preteklo
    proc = end - start;
    printf("Proces3: %d \n",proc);
    start = end;
			    
    }
    close(fbfd);
    free(buffer);
    return 0;
}

int main(void){
    
    // spremenljivke za file diskriptore cevi
    int fd_vhod[2], fd_izhod[2];
    
    // spremenljika za shranjevanje process id
    pid_t pid; 
    
    // naredimo cev za vhod
    if ( pipe(fd_vhod) == -1 ){
	printf("error making pipe vhod");
	return 1;
	}
    // naredimo cev za izhod
    if ( pipe(fd_izhod) == -1 ){
	printf("error making pipe izhod");
	return 1;
	}
    
    // nareidmo fork
    pid = fork();
    if ( pid == -1 ){
	printf("error fork");
	return 1;
    }
    // preverimo če smo v otroku
    if (pid == 0){  
	prvi( fd_vhod);
    }

    // naredimo nov proces
    pid = fork();
    if ( pid == -1 ){
	printf("error fork");
	return 1;
    }
    // preverimo če smo v otroku
    if (pid == 0){ 
	drugi( fd_vhod, fd_izhod );
    }
    
    
    // smo v roditelju
    tretji( fd_izhod ); 
    return 0;
}
