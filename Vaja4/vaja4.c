#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/times.h>
#include <sys/time.h>
#include <string.h>
#include <time.h>

#define BUFFER_VHOD_LEN 640*480*3 // velikost slike zajete s kamero
#define BUFFER_IZHOD_LEN 1280*1024*4 // velikost slike ki bo prikazana na zaslonu


int prvi(char * vhod_ptr, int vhod_sem) {
    /*
     * Prvi proces.
     *
     * char * vhod_ptr: pointer do deljenega pomnilnika za shranjevanje vhodne slike
     * int vhod_sem: disktriptor do semaforja za vhodno sliko
     *
     */
     // incualizacija spremenljik za merjenje časa
    clock_t proc, start, end;
    
    // incilizacija spremenljivke za delo s semaforjem
    struct sembuf Semaphore;

    // incializiramo spremenljivko za file diskriptorje
    int sourcefile;

    // odpremo dototeko iz katere beremo sliko kamere
    sourcefile = open("/dev/video0",O_RDONLY);

    // drugi vir slike za preizkušanje
    //sourcefile = open("/dev/urandom", O_RDONLY);

    if(sourcefile == -1)
    {
       printf("problem opening /dev/video0. \n");
       return 1;
    }

    printf("starting prvi \n");
    
    //Shranimo prvi CPU čaas
    start = clock();

    while (1) {
    
    // Določimo vrednosti sembuf
    // Ker Želimo začeti delati s deljenim pomnilnikom damo vrednost sem_op -1
    Semaphore.sem_num = 1;
    Semaphore.sem_op = -1;
    Semaphore.sem_flg = 0;
    
    // Atomična operacija za delo s semaforjem
    semop(vhod_sem, &Semaphore, 1);
	
    // preberemo BUFFER_VHOD_LEN število bytov in jih zapišemo v deljeni pomnilnik
    int n=read(sourcefile,vhod_ptr,BUFFER_VHOD_LEN);
    
    // Foločimo vrednosti sembuf
    Semaphore.sem_num = 0;
    Semaphore.sem_op = 1;
    Semaphore.sem_flg = 0;
    
    // Izvedemo atomično operacijo za delo s semaforjem
    semop(vhod_sem, &Semaphore, 1);

    // izpis za debugiranje
    //printf("%d bytes written to shared mem\n");

    // Z lseek se prestavimo na začetek dototeke /dev/video0
    lseek(sourcefile, SEEK_SET, 0);
    
    // Shranimo čas ob koncu zanke
    end = clock( );
    
    // Izračunamo koliko časa je preteklo ter vrednost izpišemo
    proc = end - start;
    printf("Proces1: %d \n",proc);
    start = end;
    }
    close(sourcefile);
    return 0;
}



int drugi(char *vhod_ptr, char *izhod_ptr, int vhod_sem, int izhod_sem)
{
    /*
     *  Drugi proces.
     *
     * char * vhod_ptr: pointer do deljenega pomnilnika za vhodno sliko
     * char * izhod_ptr: pointer do deljenega pomnilnika za izhodno sliko
     * int vhod_sem: diskriptor do semaforja za vhodno sliko
     * int izhod_sem: diskriptor do semaforja za izhodno sliko
     *
     */
     
    // incualizacija spremenljick za merjenje časa
    clock_t proc, start, end;

    // incializiramo spreenljivke za shranjevanje slik in alociramo
    // potreben prostor na heapu
    char* buffer_vhod;
    char* buffer_izhod;
    
    buffer_vhod = (char*) malloc(BUFFER_VHOD_LEN);
    buffer_izhod = (char*) malloc(BUFFER_IZHOD_LEN);
    
    // incializiramo spremenljivke za upravljanje z semaforji
    struct sembuf Semaphore_vhod;
    struct sembuf Semaphore_izhod;

    printf("starting drugi \n");
    
    // shranimo prvi CPU čas
    start = clock();
    
    while (1) {

    // Določimo vrednosti sembuf
    Semaphore_vhod.sem_num = 0;
    Semaphore_vhod.sem_op = -1;
    Semaphore_vhod.sem_flg = 0;
    
    semop(vhod_sem, &Semaphore_vhod, 1);
    
    // Vrednosti iz deljenega pomnilnika kopiramo na alicoan pomnilnik v 
    // trenutnem procesu, na ta način lahko hitro predamo kontrolo nad pomnilnikom 
    // drugem procesu
    memcpy(buffer_vhod, vhod_ptr, BUFFER_VHOD_LEN);
        
    Semaphore_vhod.sem_num = 1;
    Semaphore_vhod.sem_op = 1;
    Semaphore_vhod.sem_flg = 0;
    
    semop(vhod_sem, &Semaphore_vhod, 1);
    
    
    // Za vsak pixel na ekranu preverimo, če "pripada" sliki
    for (int pixel_num = 0; pixel_num < BUFFER_IZHOD_LEN/4; pixel_num++){
	if (pixel_num%1280 > 640) {
	    buffer_izhod[pixel_num*4] = 0;
	    buffer_izhod[pixel_num*4+1] = 0;
	    buffer_izhod[pixel_num*4+2] = 0;
	    buffer_izhod[pixel_num*4+3] = 0;
	}
	else if (pixel_num > 1280*480) {
	    buffer_izhod[pixel_num*4] = 0;
	    buffer_izhod[pixel_num*4+1] = 0;
	    buffer_izhod[pixel_num*4+2] = 0;
	    buffer_izhod[pixel_num*4+3] = 0;
	}
	else {
	    // zaporedno stevilo pixla na ekranu pretvorimo v zaporedno
	    // stevilo pixla na sliki
	    int y = pixel_num/1280;
	    int x = pixel_num%1280;
	    int pixel_num_vhod = y*640+x;

	    // V buffer_izhod shranimo vrednost pixla, ki ga zajamemo s
	    // kamero, vrednost alpha zapolnimo poljubno
	    const char red = buffer_vhod[pixel_num_vhod*3];
	    const char green = buffer_vhod[pixel_num_vhod*3 + 1];
	    const char blue = buffer_vhod[pixel_num_vhod*3 + 2];
	    buffer_izhod[pixel_num*4] = blue;
	    buffer_izhod[pixel_num*4+1] = green;
	    buffer_izhod[pixel_num*4+2] = red;
	    buffer_izhod[pixel_num*4+3] = 0;
	}
    }
    
    // Preurejeno sliko kopiramo v deljeni pomnilnik
    Semaphore_izhod.sem_num = 1;
    Semaphore_izhod.sem_op = -1;
    Semaphore_izhod.sem_flg = 0;
    
    semop(izhod_sem, &Semaphore_izhod, 1);
    
    memcpy(izhod_ptr, buffer_izhod, BUFFER_IZHOD_LEN);
    
    Semaphore_izhod.sem_num = 0;
    Semaphore_izhod.sem_op = 1;
    Semaphore_izhod.sem_flg = 0;
    
    semop(izhod_sem, &Semaphore_izhod, 1);

    // izpis za debugiranje
    //printf("%d bytes written to destfile\n");
    
    // shranimo CPU čas ob koncu procesa
    end = clock( );
    
    // izračunamo pretečeni CPU čas
    proc = end - start;
    printf("Proces2: %d \n",proc);
    start = end;
    
    }
    free(buffer_vhod);
    free(buffer_izhod);
    return 0;
}


int tretji(char *izhod_ptr, int izhod_sem) {
    /*
     * Tretji proces.
     *
     * char * izhod_ptr: pointer do deljenega pomnilnika za izhodno sliko
     * int izhod_sem: diskriptor za semafor
     *
     */
     
    // incualizacija spremenljick za merjenje časa
    clock_t proc, start, end;
    
    double FPS;    
    
    struct timeval tv;
    long start_real, end_real;

    // incializiramo spreenljivke za shranjevanje slik in alociramo
    // potreben prostor na heapu
    char* buffer;
    buffer = (char*) malloc(BUFFER_IZHOD_LEN);
    
    // Incializiramo spremenljivke za delo z semaforjem
    struct sembuf Semaphore_izhod;
    
    // incializiramo spremenljivke za file diskriptorje
    int fbfd = 0;

    printf("starting tretji \n");

    // Odpremo dototeko /dev/fb0
    fbfd = open("/dev/fb0", O_RDWR);
    if (!fbfd) {
	printf("Error: cannot open framebuffer device.\n");
	return 1;
    }
    
    // shranio prvi realni čas
    gettimeofday(&tv, NULL);
    start_real = tv.tv_usec + 1000000*tv.tv_sec;
    
    // shranimo prvi CPU čas
    start = clock();
    
    while (1) {

    Semaphore_izhod.sem_num = 0;
    Semaphore_izhod.sem_op = -1;
    Semaphore_izhod.sem_flg = 0;
    
    semop(izhod_sem, &Semaphore_izhod, 1);
    
    // kopiramo vrednosti v deljenem pomnilniku v alociran pomnilnik znotraj našega procesa
    memcpy(buffer, izhod_ptr, BUFFER_IZHOD_LEN);
    
    Semaphore_izhod.sem_num = 1;
    Semaphore_izhod.sem_op = 1;
    Semaphore_izhod.sem_flg = 0;
    
    semop(izhod_sem, &Semaphore_izhod, 1);
    
    // vrednosti zapišemo v frame buffer
    int out = write(fbfd, buffer, BUFFER_IZHOD_LEN);
    
    //printf("%d bytes written to fb0 \n", out);

    // z lseek se prestavimo na začetek /dev/fb0
    lseek(fbfd, SEEK_SET, 0);
    
    
    // shranimo CPU čas ob koncu zanke
    end = clock();
    
    // shranimo realni čas ob koncu zanke
    //end_real = times(NULL);
    gettimeofday(&tv, NULL);
    end_real = tv.tv_usec + 1000000*tv.tv_sec;
    
    // iz razlik realnega časa izračunamo FPS
    FPS = 1./(double)(end_real - start_real)*1000000;
    printf("FPS: %f \n",FPS);
    start_real = end_real;
        
    // izračunamo koliko CPU časa je preteklo
    proc = end - start;
    printf("Proces3: %d \n",proc);
    start = end;	    
    }
    close(fbfd);
    free(buffer);
    return 0;
}

int main(void){

    // spremenljivke za file diskriptorja deljenega pomnilnika
    int vhod, izhod;
    
    // spremenljivka za shranjevanje semafor discriptorja
    int vhod_sem, izhod_sem;
    
    // kazalca na deljena pomnlnika
    char * vhod_ptr;
    char * izhod_ptr;
    
    int semid;
    
    // spremenljika za shranjevanje process id
    pid_t pid;

    // incializacija deljenega pomnilnika
    vhod = shmget(IPC_PRIVATE, BUFFER_VHOD_LEN, IPC_CREAT | 0666);
    izhod = shmget(IPC_PRIVATE, BUFFER_IZHOD_LEN, IPC_CREAT | 0666);
    
    // določimo da shmat kaže na deljeni pomnilnik
    vhod_ptr = shmat(vhod, NULL, 0);
    izhod_ptr = shmat(izhod, NULL, 0);
    
    // incializacija semaforjev
    // za vsak deljeni pomnilnik imamo 2 semaforja
    vhod_sem = semget(IPC_PRIVATE, 2, IPC_CREAT | 0600);
    izhod_sem = semget(IPC_PRIVATE, 2, IPC_CREAT | 0600);
    
    // arraya za shranjevanje začetnih vrednosti semaforjev
    unsigned short semArray_vhod[2], semArray_izhod[2];
    
    semArray_vhod[0] = 0;
    semArray_vhod[1] = 1;
    semArray_izhod[0] = 0;
    semArray_izhod[1] = 1;
    
    // nastavimo začetne vrednosti semfaorjev
    semctl(vhod_sem, 0, SETALL, semArray_vhod);
    semctl(izhod_sem, 0, SETALL, semArray_izhod);
     

    // nareidmo fork
    pid = fork();
    if ( pid == -1 ){
	     printf("error fork");
	     return 1;
    }
    // preverimo če smo v otroku
    if (pid == 0){
	     prvi(vhod_ptr, vhod_sem);
    }
    else {
      // naredimo nov proces
      pid = fork();
      if ( pid == -1 ){
  	     printf("error fork");
  	     return 1;
      }
      // preverimo če smo v otroku
      if (pid == 0){
  	     drugi(vhod_ptr, izhod_ptr, vhod_sem, izhod_sem);
      }
      else {
        // smo v roditelju
        tretji(izhod_ptr, izhod_sem);
      }
    }
    return 0;
}
