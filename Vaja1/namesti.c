#include<unistd.h>
#include<fcntl.h>
#include<stdlib.h>
#include<stdio.h>

#define BUFFER_LEN 1000000

int main( int argc,char *argv[] )
{
    char* buffer;
    buffer = (char*) malloc(BUFFER_LEN);
    int sourcefile, destfile, n;
    if(argc!=3)
    {
        printf("2 args are required \n");
        return 1;
    }

    sourcefile = open(argv[1],O_RDONLY);
    if(sourcefile == -1)
    {
       printf("'%s' file does not exist \n", argv[1]);
       return 1;
     }

    destfile=open(argv[2],O_WRONLY | O_CREAT | O_TRUNC  , 0641);
    if(destfile==-1)
    {
        printf("'%s' file could not be opened \n", argv[2]);
        return 1;
    }

    while((n=read(sourcefile,buffer,BUFFER_LEN)) > 0)
    {
        int out = write( destfile, buffer, n );
        printf(".");
    }

    free(buffer);
    close(sourcefile);
    close(destfile);
    printf("Copying done! \n Thank you \n");

    return 0;
}
