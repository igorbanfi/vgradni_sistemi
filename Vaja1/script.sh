# Compilamo naš program
echo "Compilamo naš program"
gcc kopiraj.c -o kopiraj

# Naredimo kopijo ls
echo "Naredimo kopijo ls z našim programom"
./kopiraj /bin/ls ls_kopija
echo ""

# Preverimo če sta dototeki res enaki s cmp
echo "Preverimo pravilnost kopije s cmp (če je kopija uspešna ne bo izpisa)"
cmp /bin/ls ls_kopija
echo ""

# Poženemp ls_kopija
echo "Poženemo kopijo ls"
./ls_kopija
