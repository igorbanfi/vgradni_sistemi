#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>

#define BUFFER_LEN 1280*1024*4 // velikost slike v izhod.raw

int main()
{
	// iniciializiramo spremenljivko za buffer in alociramo potreben
	// prostor na heapu
	char* buffer;
	buffer = (char*) malloc(BUFFER_LEN);
	
	// alociramo spremenljivke za file descriptorje
	int sourcefile, fbfd;

	printf("starting tretji \n");
	
	// odpremo dototeko /dev/fb0 v načinu za branje
	fbfd = open("/dev/fb0", O_RDWR);
	if (!fbfd) {
	printf("Error: cannot open framebuffer device.\n");
	return 1;
	}	
	
	// odpremo dototeko izhod.raw v načinu za branje
	sourcefile = open("izhod.raw",O_RDONLY);
	if(sourcefile == -1)
	{
	printf("cannot open file izhod.raw \n");
	return 1;
	}
	
	while (1) {
	
	// z lseek se postavimo na začetek dototeke
	lseek(sourcefile, SEEK_SET, 0);
	lseek(fbfd, SEEK_SET, 0);
	
	// s sourcefile prebermo BUFFER_LEN število bytov in jih zapišemo
	// v framebuffer
	int n=read(sourcefile,buffer,BUFFER_LEN);
	int out = write(fbfd, buffer, n);
	
	// Izpis za debugiranje
	printf("%d bytes writen to fb0 \n", out);
	
	// spimo za 5 sekund
	sleep(5);
	}
	
	// zapremo dototeke in sprostimo pomnilnik
	close(sourcefile);
	close(fbfd);
	free(buffer);
	return 0;
}
