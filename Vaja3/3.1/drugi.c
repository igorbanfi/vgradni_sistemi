
#include<unistd.h>
#include<fcntl.h>
#include<stdlib.h>
#include<stdio.h>

#define BUFFER_LEN_VHOD 640*480*3 // velikost slike zajete s kamere
#define BUFFER_LEN_IZHOD 1280*1024*4 // velikost slike, ki bo prikazana na zaslonu

int main()
{
    // inicializiramo pointer za shranjevanje slik in alociramo 
    // potreben prostor v Heap
    char* buffer_vhod;
    char* buffer_izhod;
    buffer_vhod = (char*) malloc(BUFFER_LEN_VHOD);
    buffer_izhod = (char*) malloc(BUFFER_LEN_IZHOD);

    // incializiramo spremenljivke za file discriptorje
    int sourcefile, destfile;
    
    printf("starting drugi\n");
    
    // odpremo vhod.raw v readonly načinu
    sourcefile = open("vhod.raw",O_RDONLY);
    if(sourcefile == -1)
    {
       printf("cannot open vhod.raw");
       return 1;
     }
    
    while (1) {
    
    // odpremo izhod.raw
    destfile=open("izhod.raw", O_CREAT | O_WRONLY, 0644);
    if(destfile==-1)
    {
        printf("Cannot open izhod.raw");
        return 1;
    }
    
    // Z Lseek se postavimo na začetek dototek
    lseek(sourcefile, SEEK_SET, 0);
    lseek(destfile, SEEK_SET, 0);
    
    // S source file preberemo BUFFER_LEN_VHOD bytov
    int n=read(sourcefile,buffer_vhod,BUFFER_LEN_VHOD);
    
    // Za vsak pixel na ekranu preverimo, če "pripada" sliki
    for (int pixel_num = 0; pixel_num < BUFFER_LEN_IZHOD/4; pixel_num++){
	if (pixel_num%1280 > 640) {
	    buffer_izhod[pixel_num*4] = 0;
	    buffer_izhod[pixel_num*4+1] = 0;
	    buffer_izhod[pixel_num*4+2] = 0;
	    buffer_izhod[pixel_num*4+3] = 0;
	}
	else if (pixel_num > 1280*480) {
	    buffer_izhod[pixel_num*4] = 0;
	    buffer_izhod[pixel_num*4+1] = 0;
	    buffer_izhod[pixel_num*4+2] = 0;
	    buffer_izhod[pixel_num*4+3] = 0;
	}
	else {
	    // zaporedno stevilo pixla na ekranu pretvorimo v zaporedno 
	    // stevilo pixla na sliki
	    int y = pixel_num/1280;
	    int x = pixel_num%1280;
	    int pixel_num_vhod = y*640+x;
	    
	    // V buffer_izhod shranimo vrednost pixla, ki ga zajamemo s 
	    // kamero, vrednost alpha zapolnimo poljubno
	    const char red = buffer_vhod[pixel_num_vhod*3];
	    const char green = buffer_vhod[pixel_num_vhod*3 + 1];
	    const char blue = buffer_vhod[pixel_num_vhod*3 + 2];
	    buffer_izhod[pixel_num*4] = red;
	    buffer_izhod[pixel_num*4+1] = green;
	    buffer_izhod[pixel_num*4+2] = blue;
	    buffer_izhod[pixel_num*4+3] = 0;
	}
    }
    
    // buffer_izhod zapišemo v dototeko
    int out = write( destfile, buffer_izhod, BUFFER_LEN_IZHOD);
    
    // s close prisilimo, da se podatki zapišejo v dototeko, in ne ostanejo
    // v medpomnilniku
    close(destfile);
    
    // printf za debugiranje
    printf("%d bytes Written to izhod.raw \n", out);
    
    // spimo za 5 sekund
    sleep(5);
}

// zapremo dototeko in sporostimo pomnilnik
close(sourcefile);
free(buffer_vhod);
free(buffer_izhod);
return 0;
}
