
#include<unistd.h>
#include<fcntl.h>
#include<stdlib.h>
#include<stdio.h>

#define BUFFER_LEN 640*480*3 // velikost zajete slike v Bytih

int main()
{
    // inicializiramo pointer za shranjevanje slike in alociramo 
    // potreben prostor v Heap
    
    char* buffer;
	buffer = (char*) malloc(BUFFER_LEN);
    
    // inicializiramo spremenljivke za file descriptorje 
    int sourcefile, destfile;
    printf("starting prvi\n");
    
    // odpremo /dev/video0 v readonly načinu
    sourcefile = open("/dev/video0",O_RDONLY);
    
    // za preizkušanje brez kamere, buffer napolnimo z naključnimi ševili
    //sourcefile = open("/dev/urandom", O_RDONLY);
    
    if(sourcefile == -1)
    {
       printf("problem opening /dev/video0. \n");
       return 1;
     }
     
     
    while (1) {
    // odpremo vhod.raw za pisanje
    destfile=open("vhod.raw", O_CREAT | O_WRONLY, 0644);
    if(destfile==-1)
    {
        printf("problem opening file vhod.raw \n");
        return 1;
    }
    
    // Z lseek se postavimo na začetek datotek, s SEEK_SET povemo, da želimo offset nastaviti, glede na začetek dototeke
	lseek(destfile, SEEK_SET, 0);
	lseek(sourcefile, SEEK_SET, 0);
    
    // iz dototeke sourcefile preberemo BUFFER_LEN število bytov, ter jih zapišemo v dest file
    int n=read(sourcefile,buffer,BUFFER_LEN);
    int out = write( destfile, buffer, n );
    
    // s ukazom close prisilimo, da se podatki dejansko zapišejo v dototeko (da ne ostanejo v medpomnilniku)
    close(destfile);	
    
    // za debugiranje, smo si izpisovali število zapisanih bytov
    printf(" %d bytes written to vhod.raw \n", n);
    
    // program počaka 5 sekund, da imata ostala programa možnost izvesti svoje branje in pisanje
    sleep(5);
}

// Zaperemo dototeko sourcefile in sprostimo pomnilnik
close(sourcefile);	
free(buffer);    
return 0;
}
