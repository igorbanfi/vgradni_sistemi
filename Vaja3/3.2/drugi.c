
#include<unistd.h>
#include<fcntl.h>
#include<stdlib.h>
#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<errno.h>
#include<string.h>

#define BUFFER_LEN_VHOD 640*480*3  // velikost vhodne slike
#define BUFFER_LEN_IZHOD 1280*1024*4  // velikost izhone slike

int main()
{
    // incializiramo spremenljivke za shranjevenje slik in alociramo 
    // potreben protsot v heap
    char* buffer_vhod;
    char* buffer_izhod;
    buffer_vhod = (char*) malloc(BUFFER_LEN_VHOD);
    buffer_izhod = (char*) malloc(BUFFER_LEN_IZHOD);

    // inciaiziramo spremenljivke za file descriptor
    int sourcefile, destfile;
    
    // poti do dototek
    char *vhod_path = "vhod";
    char *izhod_path = "izhod";
    
    // naredimo imensko cev, poti do dototek se morajo ujemati s potmi 
    // ostalih programov
    int create;
    create = mkfifo(vhod_path, 0666);
    if ((create == -1) && (errno != EEXIST)) {
        printf("error %s", strerror(errno));
        return 1;
    }
    
    create = mkfifo(izhod_path, 0666);
    if ((create == -1) && (errno != EEXIST)) {
        printf("error %s", strerror(errno));
        return 1;
    }
    printf("starting drugi\n");
    
    // odpremo vhod.raw za branje
    sourcefile = open(vhod_path,O_RDONLY);
    if(sourcefile == -1)
    {
       printf("cannot open vhod");
       return 1;
     }

    while (1) {
    // odpremo izhod.raw za pisanje
    destfile=open(izhod_path, O_WRONLY);
    if(destfile==-1)
    {
        printf("Cannot open izhod");
        return 1;
    }
    
    // preberemo BUFFER_LEN_VHOD bytov s vhod.raw
    int n=read(sourcefile,buffer_vhod,BUFFER_LEN_VHOD);
    
    
    // Za vsak pixel na ekranu preverimo, če "pripada" sliki
    for (int pixel_num = 0; pixel_num < BUFFER_LEN_IZHOD/4; pixel_num++){
	if (pixel_num%1280 > 640) {
	    buffer_izhod[pixel_num*4] = 0;
	    buffer_izhod[pixel_num*4+1] = 0;
	    buffer_izhod[pixel_num*4+2] = 0;
	    buffer_izhod[pixel_num*4+3] = 0;
	}
	else if (pixel_num > 1280*480) {
	    buffer_izhod[pixel_num*4] = 0;
	    buffer_izhod[pixel_num*4+1] = 0;
	    buffer_izhod[pixel_num*4+2] = 0;
	    buffer_izhod[pixel_num*4+3] = 0;
	}
	else {
	    // zaporedno stevilo pixla na ekranu pretvorimo v zaporedno 
	    // stevilo pixla na sliki
	    int y = pixel_num/1280;
	    int x = pixel_num%1280;
	    int pixel_num_vhod = y*640+x;
	    
	    // V buffer_izhod shranimo vrednost pixla, ki ga zajamemo s 
	    // kamero, vrednost alpha zapolnimo poljubno
	    const char red = buffer_vhod[pixel_num_vhod*3];
	    const char green = buffer_vhod[pixel_num_vhod*3 + 1];
	    const char blue = buffer_vhod[pixel_num_vhod*3 + 2];
	    buffer_izhod[pixel_num*4] = red;
	    buffer_izhod[pixel_num*4+1] = green;
	    buffer_izhod[pixel_num*4+2] = blue;
	    buffer_izhod[pixel_num*4+3] = 0;
	}
    }
    // buffer_izhod zapišemo v izhod.raw
    int out = write( destfile, buffer_izhod, BUFFER_LEN_IZHOD);
    
    // izpis za debugiranje
    printf("%d bytes writtent to izhod \n", out);
    
    // zapremo dototeko v katero pišemo
    close(destfile);
}
    close(sourcefile);

    free(buffer_vhod);
    free(buffer_izhod);
    return 0;
}
