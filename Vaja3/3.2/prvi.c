
#include<unistd.h>
#include<fcntl.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<errno.h>
#include<sys/types.h>
#include<sys/stat.h>

#define BUFFER_LEN 640*480*3 // velikost slike zajete s kamero v bytih

int main()
{
    
    // inicializiramo spremenljivko za shranjevanje slike in alociramo 
    // alociramo potreben prostor na heapu
    char* buffer;
	buffer = (char*) malloc(BUFFER_LEN);
    
    // pripravimo file descriptorje za dototeki
    int sourcefile, destfile;
    
    // pot do vhod.raw
    char * vhod_path = "vhod";
    
    // naredimo imensko cev z mkfifo
    int create = mkfifo(vhod_path, 0666);
    
    if ((create == -1) && (errno != EEXIST)) {
        printf("error %s", strerror(errno));
        return 1;
    }
    // odpremo /dev/video0 v readonly načinu
    sourcefile = open("/dev/video0",O_RDONLY);
    
    // za preizkušanje brez kamere, buffer napolnimo z naključnimi ševili
    //sourcefile = open("/dev/urandom", O_RDONLY);
    
    if(sourcefile == -1)
    {
       printf("problem opening /dev/video0. \n");
       return 1;
     }
    
    printf("starting prvi\n");
    
    while (1) {
    // odpremo dototeko vhod v načinu za pisanje
    destfile=open(vhod_path, O_WRONLY, 0666);
    if(destfile==-1)
    {
        printf("problem opening file vhod \n");
        return 1;
    }
    
    // iz kamere /dev/video0 preberemo sliko in jo shranimo v buffer
    int n=read(sourcefile,buffer,BUFFER_LEN);
    printf("writing");
    // prebrano sliko zapišemo v imensko cev
    int out = write( destfile, buffer, n );
        
    // izpis za debugiranje
    printf(" %d bytes written to vhod\n", out);
    
    // zapremo dototeko
    close(destfile);
}
close(sourcefile);
free(buffer);    
return 0;
}
