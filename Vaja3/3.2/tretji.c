#include<unistd.h>
#include<stdio.h>
#include<fcntl.h>
#include<linux/fb.h>
#include<sys/mman.h>
#include<unistd.h>
#include<fcntl.h>
#include<stdlib.h>
#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<errno.h>
#include<string.h>

#define BUFFER_LEN 1280*1024*4

int main()
{
	// incializiramo spreenljivko za shranjevenje slike in alociramo
	// potreben prostor v heap
	char* buffer;
	buffer = (char*) malloc(BUFFER_LEN);
	
	// pripravimo discriptorje za dototeki
	int sourcefile, fbfd;
	
	// pot do izhod.raw
	char *izhod_path = "izhod";
	
	// naredimo imensko cev
	int create;
	create = mkfifo(izhod_path, 0666);
    if ((create == -1) && (errno != EEXIST)) {
        printf("error %s", strerror(errno));
        return 1;
    }
	
	// odpremo /dev/fb0
	fbfd = open("/dev/fb0", O_RDWR);
	if (!fbfd) {
	printf("Error: cannot open framebuffer device.\n");
	return 1;
	}	
	
	// odpremo izhod
	sourcefile = open(izhod_path,O_RDONLY);
	if(sourcefile == -1)
	{
	printf("cannot open izhod");
	return 1;
	}
	
	
	printf("starting tretji /n");
	while (1) {
	
	// preberemo BUFFER_LEN bytov z izhod.raw in jih zapišemo v /dev/fb0
	int n = read(sourcefile, buffer, BUFFER_LEN);
	int out = write(fbfd, buffer, BUFFER_LEN);
	
	// z lseek se prestavimo na začetek dototeke /dev/fb0
	// zato da pri naslednjem pissanju pišemo od začetka
	lseek(fbfd, SEEK_SET, 0);
}
	// zapremo izhod.raw
	close(sourcefile);
free(buffer);
return 0;
}
